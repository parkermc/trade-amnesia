package net.parkermc.tradeamnesia;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.parkermc.tradeamnesia.proxy.ProxyCommon;

@Mod(modid = ModMain.MODID, useMetadata = true)
public class ModMain{
    public static final String MODID = "tradeamnesia";
    
    @SidedProxy(clientSide = "net.parkermc.tradeamnesia.proxy.ProxyClient", serverSide = "net.parkermc.tradeamnesia.proxy.ProxyServer", modId = MODID)
    public static ProxyCommon proxy;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
    	proxy.preInit(event);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	proxy.init(event);    
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    }

}

