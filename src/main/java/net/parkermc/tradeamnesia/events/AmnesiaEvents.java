package net.parkermc.tradeamnesia.events;

import com.parkermc.amnesia.events.AmnesiaEvent;
import com.parkermc.amnesia.events.IAmnesiaEvents;

import net.parkermc.tradeamnesia.trades.TradeManager;

@AmnesiaEvent
public class AmnesiaEvents implements IAmnesiaEvents{
	
	@Override
	public void normal() {
		TradeManager.instance.randomise();
	}

	@Override
	public void random() {
		TradeManager.instance.randomise();
	}

	@Override
	public void updatePost() {		
	}

	@Override
	public void updatePostClient() {
	}

	@Override
	public void cure() {		
	}
}