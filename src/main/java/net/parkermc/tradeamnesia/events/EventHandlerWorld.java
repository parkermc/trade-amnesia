package net.parkermc.tradeamnesia.events;

import net.minecraft.entity.passive.EntityVillager;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.parkermc.tradeamnesia.trades.TradeManager;

public class EventHandlerWorld {
	@SubscribeEvent
	public void onEntityJoin(EntityJoinWorldEvent event) {
		if(event.getEntity() instanceof EntityVillager) {
			if(TradeManager.instance.randomise((EntityVillager)event.getEntity())) {
				TradeManager.instance.save();
			}
		}
	}
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event) {
		if(!event.getWorld().isRemote&&event.getWorld().provider.getDimension() == 0) {
			TradeManager.instance = new TradeManager(event.getWorld());
			TradeManager.instance.load();
		}
	}
}
