package net.parkermc.tradeamnesia.proxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.parkermc.tradeamnesia.events.EventHandlerWorld;

public class ProxyCommon {
	private EventHandlerWorld eventHandlerWorldLoad = new EventHandlerWorld();
	
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(eventHandlerWorldLoad);
	}
	
	public void init(FMLInitializationEvent event) {
	}
	
	public void postInit(FMLPostInitializationEvent event) {
	}
}
