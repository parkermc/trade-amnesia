package net.parkermc.tradeamnesia.trades;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.registry.VillagerRegistry;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class TradeManager{
	private static String folder = "data";
	private static String file = "trade_amnesia.dat";
	
	public static TradeManager instance;
	
	private List<UUID> rand = new ArrayList<UUID>();
	private Random random;
	private World world;
	private Field buyingListField;
	private Method populateBuyingListM;

	public TradeManager() {	
		this.random = new Random();
	}
	
	public TradeManager(World world) {
		this();
		this.world = world;
	}
		
	public boolean load() {
		this.buyingListField = ReflectionHelper.findField(EntityVillager.class, "buyingList", "field_70963_i");
		this.buyingListField.setAccessible(true);
		this.populateBuyingListM = ReflectionHelper.findMethod(EntityVillager.class, "populateBuyingList", "func_175554_cu");
		this.populateBuyingListM.setAccessible(true);
		rand.clear();
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			
			if(filename.exists()) {
				try {
	                FileInputStream fileinputstream = new FileInputStream(filename);
	                NBTTagCompound nbttagcompound = CompressedStreamTools.readCompressed(fileinputstream);
	                fileinputstream.close();
	                this.readFromNBT(nbttagcompound.getCompoundTag("data"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return true;
			}else {
				this.randomise();
				this.randomise();
				this.save();
				return false;
			}
		}
		return false;
	}
	
	public boolean randomise(EntityVillager e) {
		if(!this.rand.contains(e.getUniqueID())) {
			if(e.isTrading()) {
				((EntityPlayerMP) e.getCustomer()).closeScreen();
			}
			int newLevel = this.random.nextInt(4); // TODO maybe lower to 4
			if(newLevel != 0) {
				VillagerRegistry.setRandomProfession(e, this.random);// Randomise the profession
			}
			// Randomise the level
			NBTTagCompound compound = new NBTTagCompound();
			e.writeEntityToNBT(compound);
			compound.setInteger("CareerLevel", newLevel);
			e.readEntityFromNBT(compound);
			try {
				this.buyingListField.set(e, null);
				this.populateBuyingListM.invoke(e);
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e1) {
				e1.printStackTrace();
			}
			rand.add(e.getUniqueID());
			return true;
		}
		return false;
	}
	
	public void randomise() {
		rand.clear();
		for(WorldServer w : this.world.getMinecraftServer().worlds) {
			for(Entity e : w.loadedEntityList) {
                if (e instanceof EntityVillager){
                	TradeManager.instance.randomise((EntityVillager)e);
                }
			}
		}
		this.save();
	}
	
	public void readFromNBT(NBTTagCompound nbt) {
		if(nbt.hasKey("rand")) {
			this.rand.clear();
			NBTTagList list = nbt.getTagList("rand", 8);
			for(int i=0;i<list.tagCount();i++) {
				this.rand.add(UUID.fromString(list.getStringTagAt(i)));
			}
		}
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		NBTTagList list = new NBTTagList();
		for(UUID u : this.rand) {
			list.appendTag(new NBTTagString(u.toString()));;
		}
		compound.setTag("rand", list);
		return compound;
	}
	
	public void save(){
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			
			try {
				if(!filename.exists()) {
					filename.createNewFile();
				}
				NBTTagCompound nbttagcompound = new NBTTagCompound();
	            nbttagcompound.setTag("data", this.writeToNBT(new NBTTagCompound()));
	            FileOutputStream fileoutputstream = new FileOutputStream(filename);
	            CompressedStreamTools.writeCompressed(nbttagcompound, fileoutputstream);
	            fileoutputstream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
